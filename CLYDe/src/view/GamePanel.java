package view;

import model.Character;
import model.Game;
import model.OurObserver;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.Attack;
import controller.Command;
import controller.Move;

public class GamePanel extends JPanel implements MouseListener, OurObserver {
	
	private Game theGame;
	private BufferedImage terrainImages[];
	private BufferedImage characterImages[];
	
	int x; 
	int y;
	
	int selectedX;
	int selectedY;
	
	int lastSelectedX;
	int lastSelectedY;
	
	Character caller;
	
	JLabel status;
	
	boolean acquireTarget;
	Command nextCommand = null;
		
    public GamePanel(Game theGame) {
    	x = 0;
    	y = 0;
    	
    	selectedX = 21;
    	selectedY = 21;
    	lastSelectedX = 21;
    	lastSelectedY = 21;
    	
    	this.theGame = theGame;
    	terrainImages = new BufferedImage[3];
    	characterImages = new BufferedImage[2];
    	
    	for(int i = 1; i < 4; i++) {
    		try {
    			terrainImages[i-1] = ImageIO.read(new File("./terrain/" + i + ".jpg"));
    		}
    		catch(IOException e) {
    			e.printStackTrace();
    			break;
    		}
    	}
    	
    	for(int i = 1; i < 3; i++) {
    		try {
    			characterImages[i-1] = ImageIO.read(new File("./characters/" + i + ".png"));
    		}
    		catch(IOException e) {
    			e.printStackTrace();
    			break;
    		}
    	}
    	
    	acquireTarget = false;
    	
    }
	
	protected void paintComponent(Graphics g) {
       Graphics2D g2 = (Graphics2D)g;
        
        // Draw Stats Panel
        g2.drawRect(620, 0, 340, 600);
        
        int dx = 60;
        int dy = 60;
        
        for(int i = 0; i< 10; i++) {
        	for(int j = 0; j < 10; j++) {
        		g2.drawImage(terrainImages[theGame.getTerrain()[i+x*10][j+y*10]-1], i*dx, j*dy, dx, dy, null);
        	}
        }
        
        for(Character j: theGame.getAllies()) {
        	if(isVisible(j)) {
        		if(j.getType() == "Lorde") 
        			g2.drawImage(characterImages[0], (j.getPositionX()-1-10*x)*dx, (j.getPositionY()-1-10*y)*dy, dx, dy, null);
        		if(j.getType() == "Warrior") 
        			g2.drawImage(characterImages[1], (j.getPositionX()-1-10*x)*dx, (j.getPositionY()-1-10*y)*dy, dx, dy, null);
        	}
        }
        
        for(Character j: theGame.getEnemies()) {
        	if(isVisible(j)) {
        		g2.drawImage(terrainImages[2], (j.getPositionX()-1-10*x)*dx, (j.getPositionY()-1-10*y)*dy, dx, dy, null);
        		if(j.getType() == "Lorde") 
        			g2.drawImage(characterImages[0], (j.getPositionX()-1-10*x)*dx, (j.getPositionY()-1-10*y)*dy, dx, dy, null);
        		if(j.getType() == "Warrior") 
        			g2.drawImage(characterImages[1], (j.getPositionX()-1-10*x)*dx, (j.getPositionY()-1-10*y)*dy, dx, dy, null);
        	}
        }
        
        if((lastSelectedX > 19) || (lastSelectedY > 19)) {
        	lastSelectedX = selectedX;
        	lastSelectedY = selectedY;
        	repaint();
        }
        else {
        	if((selectedX < 0) || selectedX > 19 || selectedY < 0 || selectedY > 19 || (selectedX < 10)&&(x!=0) || (selectedY < 10)&&(y!=0) || (selectedX > 9)&&(x!=1) || (selectedY > 9)&&(y!=1)) {
        		
        		g2.drawImage(terrainImages[theGame.getTerrain()[lastSelectedX][lastSelectedY]-1], 760, 30, dx, dy, null);
        		if(caller != null) {
        			if(theGame.getEnemies().contains(caller))
        				g2.drawImage(terrainImages[2], 760, 30, dx, dy, null);
        			if(caller.getType() == "Lorde") 
        				g2.drawImage(characterImages[0], 760, 30, dx, dy, null);
        			if(caller.getType() == "Warrior") 
        				g2.drawImage(characterImages[1], 760, 30, dx, dy, null);
        		}
        		g2.drawRect(760, 30, 60, 60);
        		selectedX = lastSelectedX;
        		selectedY = lastSelectedY;
        	}
        	else {
        		g2.drawImage(terrainImages[theGame.getTerrain()[selectedX][selectedY]-1], 760, 30, dx, dy, null);
        		lastSelectedX = selectedX;
        		lastSelectedY = selectedY;
        		caller = null;
        		for(Character j: theGame.getAllies()) {
        			if(j.getPositionX()-1 == selectedX && j.getPositionY()-1 == selectedY) {
        				caller = j;
        				if(j.getType() == "Lorde") 
        					g2.drawImage(characterImages[0], 760, 30, dx, dy, null);
        				if(j.getType() == "Warrior") 
        					g2.drawImage(characterImages[1], 760, 30, dx, dy, null);
        			}
        		}
        		for(Character j: theGame.getEnemies()) {
        			if(j.getPositionX()-1 == selectedX && j.getPositionY()-1 == selectedY) {
        				caller = j;
        				g2.drawImage(terrainImages[2], 760, 30, dx, dy, null);
        				if(j.getType() == "Lorde") 
        					g2.drawImage(characterImages[0], 760, 30, dx, dy, null);
        				if(j.getType() == "Warrior") 
        					g2.drawImage(characterImages[1], 760, 30, dx, dy, null);
        			}
        		}	
        		
        		// Draw Selected Icon
        		g2.drawRect(760, 30, 60, 60);
        	}
        }
        
        if(caller != null) {
			g2.drawString("Name: " + caller.getName(), 640, 120);
			g2.drawString("Type: " + caller.getType(), 640, 135);
			g2.drawString("Health: " + caller.getCurrentHealth(), 640, 150);
			g2.drawString("Attack: " + caller.getAttack(), 640, 165);
			g2.drawString("Move: " + caller.getMove(), 640, 180);
			g2.drawString("Inventory: " + caller.inventoryToString(), 640, 195);
			
			if(!theGame.getEnemies().contains(caller)) {
				// Draw Stats Panel
				g2.drawString("Move", 740, 225);
				g2.drawRect(625, 200, 330, 40);
				g2.drawString("Attack", 740, 275);
				g2.drawRect(625, 250, 330, 40);
				
			}
			
		  if(theGame.playerWon())
				JOptionPane.showMessageDialog(null, "Objective Complete!");
		  
		  if(theGame.AIWon())
				JOptionPane.showMessageDialog(null, "Objective Incomplete!");
		  
			
		}
        
        g2.drawString("End Turn", 740, 325);
		g2.drawRect(625, 300, 330, 40);
        
        // Vertical Lines
        for (int i = 0; i <= 600; i += 60) {
            g2.drawLine(i, 0, i, 600);
        }

        // Horizontal Lines
        for (int i = 0; i <= 600; i += 60) {
            g2.drawLine(0, i, 600, i);
        }
        
    }
	
	public boolean isVisible(Character o) {
		if(x == 0 && y == 0)
			return o.getPositionX() < 11 && o.getPositionY() < 11;
		else if(x == 1 && y == 0)
			return o.getPositionX() >= 11 && o.getPositionY() < 11;
		else if(x == 0 && y == 1)
			return o.getPositionX() < 11 && o.getPositionY() >= 11;
		else 
			return o.getPositionX() >= 11 && o.getPositionY() >= 11;
	}

	public void update() {
		System.out.println("Here");
		this.repaint();
	}

	public void up() {
		y = 0;
		this.repaint();
	}
	
	public void down() {
		y = 1;
		this.repaint();
	}
	
	public void left() {
		x = 0;
		this.repaint();
	}
	
	public void right() {
		x = 1;
		this.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		selectedX = (e.getX() - 20)/60 + 10*x;
		selectedY = (e.getY() - 45)/60 + 10*y;
		
		if(acquireTarget == true) {
			if(nextCommand instanceof Move) {
				if(e.getX() < 20 || e.getX() > 660 || e.getY() < 20 || e.getY() > 660) {
					JOptionPane.showMessageDialog(null, "That point is not on the map");
				}
				else {
					((Move)nextCommand).moveTo(selectedX+1, selectedY+1);
					nextCommand.execute(caller, caller);
					acquireTarget = false;
					repaint();
				}
			}
			if(nextCommand instanceof Attack) {
				if(e.getX() < 20 || e.getX() > 660 || e.getY() < 20 || e.getY() > 660) {
					JOptionPane.showMessageDialog(null, "That point is not on the map");
				}
				else {
					Character target = null;
					for(Character j: theGame.getEnemies()) {
						if((j.getPositionX() == (selectedX+1)) && (j.getPositionY() == (selectedY+1))) {
							target = j;
						}
					}
					if(target != null)
						nextCommand.execute(caller, target);
					acquireTarget = false;
					
					if(!theGame.getAllies().isEmpty()) {
						for(Character j: theGame.getAllies()) {
							if(j.getCurrentHealth() <= 0)
								theGame.getAllies().remove(j);
						}
					}
					
					if(!theGame.getEnemies().isEmpty()) {
						for(Character j: theGame.getEnemies()) {
							if(j.getCurrentHealth() <= 0)
								theGame.getEnemies().remove(j);
						}
					}
					
					repaint();
				}
			}
		}
		
		else if(e.getX() > 625 && e.getX() < 955 && e.getY() > 240 && e.getY() < 280) {
			if(caller!= null && theGame.getAllies().contains(caller) && !caller.ifActioned()) {
				nextCommand = new Move();
				acquireTarget = true;
			}
			else 
				JOptionPane.showMessageDialog(null, "You have already used your action");
		}
			
		else if(e.getX() > 625 && e.getX() < 955 && e.getY() > 290 && e.getY() < 330) {
			if(caller!= null && theGame.getAllies().contains(caller) && !caller.ifActioned()) {
				nextCommand = new Attack();
				acquireTarget = true;
			}
			else 
				JOptionPane.showMessageDialog(null, "You have already used your action");
		}
		
		else if(e.getX() > 625 && e.getX() < 955 && e.getY() > 340 && e.getY() < 380) {
			List<Command> enemyCommands = theGame.getAI().desiredMove(theGame);
			for(Character j: theGame.getAllies())
				j.setActioned(false);
		}
		
		else {
			if(e.getX() < 20 || e.getX() > 660 || e.getY() < 20 || e.getY() > 660);
			else {
				repaint();
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
