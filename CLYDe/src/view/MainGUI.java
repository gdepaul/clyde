package view;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.*;

public class MainGUI extends JFrame {
	
	private static GamePanel gamePanel;
	private static Game theGame;
	
	public MainGUI() {
		
		theGame = new Game();
		
		this.setSize(1000, 660);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(new Point(0, 0));
		this.setVisible(true);
				
		this.setLayout(null);
				
		gamePanel = new GamePanel(theGame);
		gamePanel.setBounds(20, 20, 961, 961);
		this.add(gamePanel);
		
		theGame.addObserver(gamePanel);
		
		this.setVisible(true);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ListenToKeys listener = new ListenToKeys();
        this.addKeyListener(listener);
        
        this.addMouseListener(gamePanel);
        
		JOptionPane.showMessageDialog(null, "Objective: " + theGame.getObjective());
		
		this.setTitle("Fire Emblem: Stage 1");

	}
	
	public static void main(String[] args) {
		JFrame myWindow = new MainGUI();
		gamePanel.repaint();
	}
	
	private class ListenToKeys implements KeyListener {

	    public void keyPressed(KeyEvent e) {
	    	int keyCode = e.getKeyCode();

		    if (keyCode == KeyEvent.VK_UP) {
		        gamePanel.up();
		      }

		    if (keyCode == KeyEvent.VK_DOWN) {
		    	gamePanel.down();
		      }

		    if (keyCode == KeyEvent.VK_LEFT) {
		        gamePanel.left();
		      }

		    if (keyCode == KeyEvent.VK_RIGHT) {
		        gamePanel.right();
		      }
	     
	    }

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			
		}
	  }
	
}
