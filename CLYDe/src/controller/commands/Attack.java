package controller;
import javax.swing.JOptionPane;

import model.Character;

public class Attack implements Command {
	//private int strength;
	//private Character caller;
	private Character caller;
	private Character target;
	private int callerCurrentHealth;
	private int targetCurrentHealth;
	
	/*public Attack(int strength){
		//this.caller = caller;
		this.strength = strength;
	}*/

	@Override
	public void execute(Character caller, Character target) {
		this.caller = caller;
		this.target = target;
		callerCurrentHealth = caller.getCurrentHealth();
		targetCurrentHealth = target.getCurrentHealth();
		//Using caller to determine whether it can be executed.
		//Some animation about attacking
		caller.setTarget(target);
		target.setTarget(caller);
		
		if(caller.isTargetInAttackRange()){
			caller.attack();
			//Animation here
			if (target.ifAlive()){
				if(target.isTargetInAttackRange()){
					target.attack();
					//Animation here
					if(!caller.ifAlive()){
						//Delete Character
					}
				}
			}else{
				//Delete Character
			}
		}else{
			JOptionPane.showMessageDialog(null, "Not in Range.");
		}
	}

	@Override
	public void undo() {
		caller.setCurrentHealth(callerCurrentHealth);
		target.setCurrentHealth(targetCurrentHealth);
	}
	
}
