package controller;

import javax.swing.JOptionPane;

import model.Character;

public class Move implements Command {
	//private int movingDistance;
	private Character caller;
	private int oldX, oldY;
	private int newX, newY;
	
	/*public Move(int x, int y){
		newX = x;
		newY = y;
	}*/

	@Override
	public void execute(Character caller, Character target) {
		this.caller = caller;
		oldX = caller.getPositionX();
		oldY = caller.getPositionY();
		if(caller.isInMoveRange(newX, newY)){
			caller.setPosition(newX, newY);
		}else{
			JOptionPane.showMessageDialog(null, "You cannot go there!");
		}
	}

	@Override
	public void undo() {
		caller.setPosition(oldX, oldY);
	}
	
	public void moveTo(int newX, int newY){
		this.newX = newX;
		this.newY = newY;
	}

}
