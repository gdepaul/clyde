package controller;
import model.Character;

public class Heal implements Command {
	private int strength;
	//private Character caller;
	private Character target;
	
	public Heal(int strength){
		//this.caller = caller;
		this.strength = strength;
	}

	@Override
	public void execute(Character caller, Character target) {
		this.target = target;
		int tempHealth = target.getHealth() + strength;
		target.setCurrentHealth(tempHealth);
		//some animation about healing
	}

	@Override
	public void undo() {
		int tempHealth = target.getHealth() - strength;
		target.setCurrentHealth(tempHealth);
	}

}
