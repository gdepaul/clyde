package controller;

import model.Character;
import model.Item;

public class UseItem implements Command {
	private Item item;
	
	public UseItem(Item item){
		this.item = item;
	}

	@Override
	public void execute(Character caller, Character target) { //if the item is use to caller, then the target is also caller itself
		
	}

	@Override
	public void undo() {
		
	}

}
