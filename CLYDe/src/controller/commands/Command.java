package controller;

import model.Character;

public interface Command {
	
	public void execute(Character caller, Character target);
	
	public void undo();
	
	//public void setStrangth(int strangth);
	
	//public int getStrangth();

}
