package controller.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Character;
import model.Map;
import controller.commands.Trade;

public class TradeButtonListener implements ActionListener {
	private Map map;
	
	public TradeButtonListener(Map map){
		this.map = map;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//Should display all item to screen.
		//Player will click item from screen and then return the number of item.
		Character ally = map.getSelectedAlly();
		Character tradeToAlly = map.getTradeOrHealAlly();
		Trade trading = new Trade();
		trading.execute(ally, tradeToAlly);
	}

}
