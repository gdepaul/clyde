package controller.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.commands.Move;
import model.Map;

public class MoveButtonListener implements ActionListener {
	private Map map;
	
	public MoveButtonListener(Map map){
		this.map = map;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Move moving = new Move(); 
		moving.moveTo(map.getMoveToX(), map.getMoveToY());
		moving.execute(map.getSelectedAlly(), map.getSelectedAlly());
	}

}
