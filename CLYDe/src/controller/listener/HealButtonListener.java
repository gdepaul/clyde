package controller.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.commands.Heal;
import model.Map;

public class HealButtonListoner implements ActionListener {
	private Map map;
	
	public HealButtonListoner(Map map){
		this.map = map;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Heal healing = new Heal();
		healing.execute(map.getSelectedAlly(), map.getTradeOrHealAlly());
	}

}
