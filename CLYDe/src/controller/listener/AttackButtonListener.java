package controller.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.commands.Attack;
import controller.commands.Command;
import controller.commands.Move;
import model.Map;

public class AttackButtonListener implements ActionListener {

	private Map map;
	
	public AttackButtonListener(Map map){
		this.map = map;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		//Still have some problem that how far need to move? Moving and attacking is in different round or in same round?
		Move moving = new Move();
		moving.moveTo(map.getMoveToX(), map.getMoveToY());
		moving.execute(map.getSelectedAlly(), map.getSelectedEnemy());
		Command attacking = new Attack();
		attacking.execute(map.getSelectedAlly(), map.getSelectedEnemy());
	}

}
