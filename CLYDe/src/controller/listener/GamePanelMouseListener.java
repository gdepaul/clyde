package controller.listener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import model.Map;
import model.Character;
import view.GamePanel;

public class GamePanelMouseListener implements MouseListener {
	int x; 
	int y;
	int selectedX;
	int selectedY;
	
	/*private Character ally = null;
	private Character enemy = null;*/
	
	private GamePanel gamePanel;
	private Map map;
	
	public GamePanelMouseListener(GamePanel gamePanel, Map map){
		this.gamePanel = gamePanel;
		this.map = map;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		selectedX = (e.getX() - 20)/60 + 10*x;
		selectedY = (e.getY() - 45)/60 + 10*y;
		
		if(map.getSelectedAlly() == null){
			ArrayList<Character> allies = map.getAllies();
			for(Character ally : allies ){
				if(ally.getPositionX() == selectedX && ally.getPositionY() == selectedY){
					map.setSelectedAlly(ally);
					break;
				}
			}
		}else if(map.getSelectedEnemy() == null){
			ArrayList<Character> enemies = map.getEnemies();
			for(Character enemy : enemies ){
				if(enemy.getPositionX() == selectedX && enemy.getPositionY() == selectedY){
					map.setSelectedEnemy(enemy);
					break;
				}
			}
		}else{
			if(map.getMoveToX() == -1 || map.getMoveToY() == -1){
				map.setMoveToX(selectedX);
				map.setMoveToY(selectedY);
			}
		}
		
		/*if(ally != null && ally != null){
			Move moving = new Move();
			moving.moveTo(selectedX, selectedY);
			moving.execute(ally, ally);
			Command attacking = new Attack();
			attacking.execute(ally, enemy);
		}*/
		
		System.out.println(selectedX + " " + selectedY);
		
		if(selectedX < 0 || selectedX > 20 || selectedY < 0 || selectedY > 20);
		else
			gamePanel.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
