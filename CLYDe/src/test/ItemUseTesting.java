package test;

import model.*;
import model.Character;
import static org.junit.Assert.*;

import org.junit.Test;

public class ItemUseTesting {

	@Test
	public void Elixir() {
		System.out.println();
		Character Greg = new Archer("Greg");
		Greg.addItem(new Elixir());
		int initialHealth = Greg.getHealth();
		System.out.println("Before: " + Greg.inventoryToString());
		Greg.useItem(0);
		int finalHealth = Greg.getHealth();
		System.out.println("After: " + Greg.inventoryToString());
		//assertTrue(finalHealth == initialHealth + 15);
		assertTrue(finalHealth == initialHealth);
	}
	
	
	@Test
	public void PowerRing() {
		System.out.println();
		Character Greg = new Archer("Greg");
		Greg.addItem(new PowerRing());
		int initialAttack = Greg.getAttack();
		System.out.println("Before: " + Greg.inventoryToString());
		Greg.useItem(0);
		int finalAttack = Greg.getAttack();
		System.out.println("After: " + Greg.inventoryToString());
		//assertTrue(finalAttack == initialAttack + 15);
		assertTrue(finalAttack == initialAttack + 5);
	}
	
	@Test
	public void Boots() {
		System.out.println();
		Character Greg = new Archer("Greg");
		Greg.addItem(new Boots());
		int initialMovement = Greg.getMove();
		System.out.println("Before: " + Greg.inventoryToString());
		Greg.useItem(0);
		int finalMovement = Greg.getMove();
		System.out.println("After: " + Greg.inventoryToString());
		//assertTrue(finalMovement == initialMovement + 15);
		assertTrue(finalMovement == initialMovement + 2);
	}
	
	@Test
	public void HoldingMultipleItems() {
		System.out.println();
		Character Greg = new Archer("Greg");
		Greg.addItem(new Boots());
		Greg.addItem(new PowerRing());
		Greg.addItem(new Elixir());
		Greg.addItem(new PowerRing());
		System.out.println("Holding Four Items: " + Greg.inventoryToString());
	}

}
