package test;


import static org.junit.Assert.*;
import model.Elixir;
import model.FlyingKnight;
import model.Lorde;
import model.Character;
import model.Item;
import model.Mage;
import model.Warrior;

import org.junit.Test;

public class characterTest {
	
	@Test
	public void testConstructer(){
		Character c1 = new Lorde("lorde");
		assertTrue(c1.getAttack() == 7);
		assertTrue(c1.getHealth() == 27);
		assertTrue(c1.getMove() == 5);
	}
	
	@Test
	public void testAttack(){
		Character c2 = new FlyingKnight("flyingknight");
		Character c3 = new Warrior("warrior");
		c2.setTarget(c3);
		c3.setTarget(c2);
		System.out.println(c2.getCurrentHealth() + " " + c3.getCurrentHealth());
		c2.attack();
		System.out.println(c2.getCurrentHealth() + " " + c3.getCurrentHealth());
		assertTrue(c3.ifAlive() == true);
		c2.attack();
		System.out.println(c2.getCurrentHealth() + " " + c3.getCurrentHealth());
		c2.attack();
		System.out.println(c2.getCurrentHealth() + " " + c3.getCurrentHealth());
		c2.attack();
		System.out.println(c2.getCurrentHealth() + " " + c3.getCurrentHealth());
		assertTrue(c3.ifAlive() == false);
		c3.setHealth(20);
	}
	
	@Test
	public void testStatus(){
		Character c4 = new Mage("mage");
		Character c3 = new Warrior("warrior");
		c4.setTarget(c3);
		c3.setTarget(c4);
		System.out.println(c4.statusToString());
		assertTrue(c4.getCurrentHealth() == 21);
		c4.addItem(new Elixir());
		c3.attack();
		System.out.println(c4.statusToString());
		assertTrue(c4.getCurrentHealth() == 15);
		c4.useItem(0);
		System.out.println(c4.statusToString());
		assertTrue(c4.getCurrentHealth() == 21);
	}
}
