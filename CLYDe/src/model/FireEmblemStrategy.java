package model;
import controller.*;
import view.*;

import java.awt.Point;
import java.util.List;
import java.util.ArrayList;

public interface FireEmblemStrategy {

		// The ComputerPlayer has access to "seeing" anything about
		// the game when it is given the game as an argument.
		public List<Command> desiredMove(Game theGame); //may want to change command to move
}
