package model;

import java.util.ArrayList;

public interface Map {
	public int[][] getMap();
	public ArrayList<Character> getAllies();
	public ArrayList<Character> getEnemies();
	public void setSelectedAlly(Character ally);
	public void setSelectedEnemy(Character enemy);
	public void setTradeOrHealAlly(Character ally);
	public void setMoveToX(int selectedX);
	public void setMoveToY(int selectedY);
	
	public Character getSelectedAlly();
	public Character getSelectedEnemy();
	public Character getTradeOrHealAlly();
	public int getMoveToX();
	public int getMoveToY();
}
