package model;

public class Boots extends Item {
	
	public Boots() {
		super("Boots");
	}

	public void useItem(Character user) {
		user.setMove(2);
	}
	
}