package model;

public class Archer extends Character {

	public Archer(String name) {
		super(name, "Archer", 22, 7, 5, 2);
	}
	
	@Override
	//rewrite this class for Archer, as Archer can only attack in distance;
	public boolean isTargetInAttackRange(){
		Character target = super.getTarget();
		int distance = Math.abs(this.getPositionX() - target.getPositionX()) 
				+ Math.abs(this.getPositionY() - target.getPositionY());
		if(distance == this.getAttackRange())
			return true;
		else 
			return false;
	}
	
}
