package model;

import java.awt.Point;
import java.util.List;

import controller.*;
import view.*;

public class ComputerPlayer {

	  private FireEmblemStrategy myStrategy;

	  private String name;

	  public ComputerPlayer(String name) {
	    this.name = name;
	    // This default can be changed with setStrategy
	    //myStrategy = new RandomAI(); ****************************
	    myStrategy = new WeakAI();
	  }

	  /**
	   * Change the AI for this ComputerPlayer
	   * @param stategy
	   */
	  public void setStrategy(FireEmblemStrategy strategy) {
	    myStrategy = strategy;
	  }

	  /**
	   * Delegate to my strategy, which can "see" the game for my next move
	   * 
	   * @param theGame The current state of the game when asked for a move
	   * 
	   * @return A java.awt.Point that store two ints: an x and a y
	   */
	  public List<Command> desiredMove(Game theGame) {
	    return myStrategy.desiredMove(theGame);
	  }

	  /**
	   * Yes, you know what this does.
	   * @return
	   */
	  public String getName() {
	    return name;
	  }
}
