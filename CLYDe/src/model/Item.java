package model;

public abstract class Item {
	
	private String name;
	private int x, y;
	
	public Item(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getPositionX() {
		return x;
	}
	
	public int getPositionY() {
		return y;
	}
	
	public abstract void useItem(Character user);
}
