package model;

public class Elixir extends Item {
	
	public Elixir() {
		super("Elixir");
	}

	public void useItem(Character user) {
		user.setHealth(15);
	}
	
}
