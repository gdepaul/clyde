package model;

import java.util.ArrayList;
import java.util.Observable;

public class Game extends OurObservable {
	private Map1 map;
	private ArrayList<Character> playerCharacters;
	private ArrayList<Character> AICharacters;
	private int turn;
	private ComputerPlayer enemy = new ComputerPlayer("Evil Genius");
	
	public Game(){
		map = new Map1();
		initializePlayerCharacters();
		initializeAICharacters();
		turn = 1;
		enemy.setStrategy(new WeakAI());
	}
	
	public int[][] getTerrain() {
		return map.getMap();
	}

	public ArrayList<Character> getAllies() {
		return playerCharacters;
	}
	
	public ArrayList<Character> getEnemies() {
		return AICharacters;
	}
	
	public void printStateOfUnits() {
		System.out.println("ALLIED UNITS:");
		for(Character a : playerCharacters) {
			System.out.print(a.statusToString() + "Position: (" + a.getPositionX() + ", " + a.getPositionY() + ")\n" );
		}
		System.out.println("ENEMY UNITS:");
		for(Character e : AICharacters) {
			System.out.print(e.statusToString() + "Position: (" + e.getPositionX() + ", " + e.getPositionY() + ")\n\n" );
		}
	}
	
	private void initializePlayerCharacters() {
		playerCharacters = map.getAllies();
		
	}

	private void initializeAICharacters() {
		AICharacters = map.getEnemies();
	}
	
	//check if all character in this turn has actioned;
	//if true, then active next turn's characters;
	public boolean ifTurnEnd(){
		ArrayList<Character> thisTurn;
		ArrayList<Character> nextTurn;
		if ( turn % 2 == 0){
			thisTurn = this.AICharacters;
			nextTurn = this.playerCharacters;
		}else{
			thisTurn = this.playerCharacters;
			nextTurn = this.AICharacters;
		}
		
		for(int i = thisTurn.size(); i != 0; i--){
			if(!thisTurn.get(i).ifActioned())
				return false;
		}
		
		ActiveNextTurnCharacter(thisTurn, nextTurn);
		return true;
	}
	
	//if we have a "next turn" button on GUI, we can use this to end our turn;
	public void nextTurnClicked(){
		ActiveNextTurnCharacter(this.playerCharacters, this.AICharacters);
	}

	//inactive this turn's characters, and active next turn's characters;
	private void ActiveNextTurnCharacter(ArrayList<Character> thisTurn, ArrayList<Character> nextTurn) {
		for(Character c : nextTurn)
			c.setActioned(false);
		for(Character c : thisTurn)
			c.setActioned(false);
		turn ++;
	}
	
	//if player lost its Lords, then AI wins the game;
	public boolean AIWon(){
		for(Character c : playerCharacters)
			if(c.getType().equals("Lorde") && !c.ifAlive())
				return true;
		return false;		
	}
	
	//if player destroys all AI's characters, player wins;
	//we may have other winning situations in iteration 2;
	public boolean playerWon(){
		if(destroyAll(this.AICharacters))
			return true;
		return false;
	}
	
	//check is all the characters have been destroyed 
	private boolean destroyAll(ArrayList<Character> characters){
		for(Character c : characters)
			if(c.ifAlive())
				return false;
		return true;
	}
	
	public void selectMove(Character o) {
		o.setPosition(o.getPositionX()+1, o.getPositionY() + 1);
		notifyObservers();
	}
	
	public String getObjective() {
		return map.getObjective();
	}
	
	public ComputerPlayer getAI() {
		return enemy;
	}

}
