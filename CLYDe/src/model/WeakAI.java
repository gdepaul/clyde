package model;

import controller.*;
import view.*;

import java.awt.Point;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class WeakAI implements FireEmblemStrategy{
	  private static Random generator;

	  public WeakAI() {
	    generator = new Random();
	  }

	  public boolean isAvailable(Game theGame, int h, int k) {
		  if(h<1 || h>18)
			  return false;
		  if(k<1 || k>18)
			  return false;
		  for(Character ally : theGame.getAllies()) {
			  if(ally.getPositionX()==h && ally.getPositionY()==k)
				  return false;
		  }
		  for(Character enemy : theGame.getEnemies()) {
			  if(enemy.getPositionX()==h && enemy.getPositionY()==k)
				  return false;
		  }
		  return true;
	  }
	  
	  @Override
	  public List<Command> desiredMove(Game theGame) { //may want to change Command to Move
		  System.out.println("Hello, I am a weak computer player.");
		  
		  List<Command> computerMoves = new ArrayList<Command>(); //for now, only move and wait commands
		  //int x2, y2;
		  for(Character c : theGame.getEnemies()) { //need to get computerCharacters from theGame
			  if(isAvailable(theGame, c.getPositionX(), c.getPositionY()-1)) {
				  Move nextCommand = new Move();
				  ((Move)nextCommand).moveTo(c.getPositionX(), c.getPositionY()-1);
				  nextCommand.execute(c, c);
				//  theGame.setUnavailable(c.getPositionX(), c.getPositionY()-1);
				//  theGame.setAvailable(c.getPositionX(), c.getPositionY());
				  computerMoves.add(nextCommand);
			  }
			  else if(isAvailable(theGame, c.getPositionX()+1, c.getPositionY())) {
				  Move nextCommand = new Move();
				  ((Move)nextCommand).moveTo(c.getPositionX()+1, c.getPositionY());
				  nextCommand.execute(c, c);
				//  theGame.setUnavailable(c.getPositionX()+1, c.getPositionY());
				//  theGame.setAvailable(c.getPositionX(), c.getPositionY());
				  computerMoves.add(nextCommand);
			  }
			  else if(isAvailable(theGame, c.getPositionX()-1, c.getPositionY())) {
				  Move nextCommand = new Move();
				  ((Move)nextCommand).moveTo(c.getPositionX()-1, c.getPositionY());
				  nextCommand.execute(c, c);
				 // theGame.setUnavailable(c.getPositionX()-1, c.getPositionY());
				  //theGame.setAvailable(c.getPositionX(), c.getPositionY());
				  computerMoves.add(nextCommand);
			  }
			  else if(isAvailable(theGame, c.getPositionX(), c.getPositionY()+1)) {
				  Move nextCommand = new Move();
				  ((Move)nextCommand).moveTo(c.getPositionX(), c.getPositionY()+1);
				  nextCommand.execute(c, c);
				  //theGame.setUnavailable(c.getPositionX(), c.getPositionY()+1);
				  //theGame.setAvailable(c.getPositionX(), c.getPositionY());
				  computerMoves.add(nextCommand);
			  }
			  else { //no wait Command, so moving to same square for same effect
				  System.out.println("I'm doing nothing this turn.");
				 // theGame.setAvailable(c.getPositionX(), c.getPositionY());
				 // Move nextCommand = new Move();
				  //((Move)nextCommand).moveTo(c.getPositionX(), c.getPositionY());
				  //nextCommand.execute(c, c);
				  //computerMoves.add(nextCommand);
				  //theGame.setUnavailable(c.getPositionX(), c.getPositionY());
			  }
			  c.setActioned(true);
			  //theGame.setChanged(); //MAY NOT NEED THIS*************************************
			  theGame.notifyObservers();
			  theGame.printStateOfUnits();
		  }
		  System.out.println("AI turn end.");
		  
		  return computerMoves;
	  }
}
