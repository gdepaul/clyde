package model;

public class PowerRing extends Item {
	
	public PowerRing() {
		super("Power Ring");
	}

	public void useItem(Character user) {
		user.setAttack(5);
	}
	
}